from argparse import ArgumentParser

from datasource import DataSource
import sys
from util import read_sentence, LANGUAGES

from masked_sentence_generator import gen_sentences


def parse_options(args):
    parser = ArgumentParser('POS/MS-Test')
    parser.add_argument('language', metavar='LANG', choices=LANGUAGES, help='language of the model')
    parser.add_argument('model', metavar='MODELNAME', help='name of the model')
    parser.add_argument('file', metavar='FILENAME', help='Filename (without extension)')
    return parser.parse_args(args)


def mask_debugger(args):
    ds = DataSource(args.language, args.model)
    tf, pos = ds.get_text_file(args.file)

    def show_pos_and_masked_sentences(s_id):
        r = gen_sentences(ds.lang, tf[s_id], pos[s_id])
        print(str(pos[s_id]))
        for s in r:
            print(' '.join(s.sentence))
            print('off: {}, len: {}, result: `{}`'.format(s.offset, s.length, tf[s_id].text[s.offset:s.offset + s.length]))
            print('\n')
    read_sentence(show_pos_and_masked_sentences, len(tf))


if __name__ == '__main__':
    mask_debugger(parse_options(sys.argv[1:]))
