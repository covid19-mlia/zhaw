from argparse import ArgumentParser
import sys
import os
from collections import defaultdict
import time
from multiprocessing import Pool
from torch.multiprocessing import Pool as TorchPool, set_start_method
import itertools
from typing import Dict, List, Union

from MaskedWordPredictor import MaskedWordPredictor
from clustering import Clustering

from datasource import DataSource
from masked_sentence_generator import gen_sentences
from util import MaskedSentence, chunks, Annotation, LANGUAGES, read_sentence


LABELS = {
    'B': 'behavior',
    'D': 'drug-trt',
    'F': 'findings',
    'L': 'legal-reg',
    'O': 'other',
    'S': 'sosy-dis',
    'T': 'tests'
}

GPU_BATCH_SIZE = 250

def parse_options(args):
    parser = ArgumentParser('NER Runner')
    parser.add_argument('language', metavar='LANG', choices=LANGUAGES, help='language of the model')
    parser.add_argument('model', metavar='MODELNAME', help='name of the model')
    parser.add_argument('-s', '--subdir', help='sub directory in the language directory', default='')
    subparsers = parser.add_subparsers(required=True)

    labeling_parser = subparsers.add_parser('label', help='Label files (main run, create annotations)')
    labeling_parser.add_argument('-f', '--files', nargs='+', required=True,
                                 help='Filenumbers to process (without extension)')
    labeling_parser.set_defaults(func=labeling)

    inspect_parser = subparsers.add_parser('inspect', help='Inspect/count/debug prediction')
    inspect_sub_parsers = inspect_parser.add_subparsers(required=True)
    count_parser = inspect_sub_parsers.add_parser('count', help='Count predicted clusters')
    count_parser.add_argument('-d', '--debug', action='store_true', help='write debug file')
    count_parser.add_argument('-f', '--files', nargs='+', required=True,
                              help='Filenumbers to process (without extension)')
    count_parser.set_defaults(func=inspect_count)

    interact_parser = inspect_sub_parsers.add_parser('interactive', help='Interactive prediction')
    interact_parser.add_argument('-f', '--file', required=True, help='Filenumber to process (without extension)')
    interact_parser.set_defaults(func=interact)
    return parser.parse_args(args)


class NerRunner:
    def __init__(self, ds: DataSource):
        print('loading predictor...')
        self.ds = ds
        self.masked_word_predictor = MaskedWordPredictor(ds)
        print('loading clustering service...')
        self.clustering = Clustering.for_prediction(ds)
        self.text_file = None
        self.pos_file = None

    def show_files(self):
        files = self.ds.list_source_files()
        print(files)

    def load_file(self, filename):
        self.text_file, self.pos_file = self.ds.get_text_file(filename)

    def generate_masked_sentences_for_id(self, id) -> List[MaskedSentence]:
        return gen_sentences(self.ds.lang, self.text_file[id], self.pos_file[id])

    def count_clusters_for_masked_sentences(self, sentences: List[List[str]], num_predictions, num_clusters):
        clusters = self.get_clusters_for_masked_sentences(sentences, num_predictions, num_clusters)
        cluster_count = defaultdict(int)
        for s_c in clusters:
            for c in s_c:
                cluster_count[c] += 1
        return cluster_count

    def get_predictions_for_masked_sentences(self, sentences: List[List[str]], num_predictions):
        return self.masked_word_predictor.predict_masked_words(sentences, num_predictions)

    def get_clusters_for_masked_sentences(self, sentences: List[List[str]], num_predictions, num_clusters):
        predictions = self.masked_word_predictor.predict_masked_words(sentences, num_predictions)
        return self.get_clusters_for_predictions(predictions, num_clusters)
        
    def get_clusters_for_predictions(self, predictions, num_clusters):
        return [self.clustering.find_clusters(p, num_clusters) for p in predictions]

    def get_labels_for_masked_sentences(self, sentences: List[List[str]], num_predictions, num_clusters,
                                        return_clusters=False):
        predictions = self.masked_word_predictor.predict_masked_words(sentences, num_predictions)
        clusters = [self.clustering.find_clusters(p, num_clusters) for p in predictions]
        cluster_labels = [self.clustering.get_cluster_labels(c) for c in clusters]
        if return_clusters:
            return [get_labels(labels) for labels in cluster_labels], clusters
        else:
            return [get_labels(labels) for labels in cluster_labels]

    def get_labels_for_predictions(self, predictions, num_clusters):
        clusters = self.get_clusters_for_predictions(predictions, num_clusters)
        cluster_labels = [self.clustering.get_cluster_labels(c) for c in clusters]
        return [get_labels(labels) for labels in cluster_labels]


runner: NerRunner = None


def get_labels(labels: List[str]) -> Union[str, None]:
    label_counts = defaultdict(int)
    for label in labels:
        if label in LABELS:
            label_counts[label] += 1
    if len(label_counts) > 0:
        return sorted(label_counts.items(), key=lambda item: item[1], reverse=True)[0][0]
    else:
        return None


def get_masked_sentences(ids) -> List[MaskedSentence]:
    result = []
    for i in ids:
        masked_sentences = runner.generate_masked_sentences_for_id(i)
        result += masked_sentences
    return result


def get_cluster_counts(masked_sentences: List[MaskedSentence]):
    sentences = [ms.sentence for ms in masked_sentences]
    clusters = runner.count_clusters_for_masked_sentences(sentences, 5, 3)
    cluster_counts = defaultdict(int)
    for k, n in clusters.items():
        cluster_counts[k] += n
    return cluster_counts


def generate_masked(line_ids) -> List[MaskedSentence]:
    print('Generating masked sentences for {} lines...'.format(len(line_ids)), end='\t')
    start = time.time()
    with Pool() as pool:
        masked_sentences = pool.map(get_masked_sentences, chunks(line_ids, 10))
    end = time.time()
    print('done ({}s)'.format(round(end - start, 2)))
    return list(itertools.chain.from_iterable(masked_sentences))


def count_predicted_clusters(masked_sentences: List[MaskedSentence]) -> List[Dict[str, int]]:
    sentence_amount = len(masked_sentences)
    print('Received {} sentences to predict'.format(sentence_amount))
    total = 0.0
    chunk_size = 1000
    for i, chunk in enumerate(chunks(masked_sentences, chunk_size)):
        n_fr = i*chunk_size
        n_to = min(sentence_amount, (i+1)*chunk_size-1)
        print('Getting predictions for sentences {:05d}-{:05d}...'.format(n_fr, n_to), end='\t')
        start = time.time()
        result = map(get_cluster_counts, chunks(chunk, GPU_BATCH_SIZE))
        end = time.time()
        print('done ({}s)'.format(round(end - start, 2)))
        total += (end - start)
        yield result
    print('Total: {}s'.format(round(total, 2)))


def count_clusters(filenames, output_file):
    for filename in filenames:
        print('Loading file {}'.format(filename))
        runner.load_file(filename)
        total_counts = defaultdict(int)
        sentences_in_file = len(runner.text_file)
        sentence_ids_chunked = chunks(range(sentences_in_file), 5000)
        for i, s in enumerate(sentence_ids_chunked):
            print('Processing sentence chunks {:06d}-{:06d} of {}'.format(i*5000, (i+1)*5000-1, sentences_in_file))
            masked_sentences = generate_masked(s)
            result = count_predicted_clusters(masked_sentences)
            for item in result:
                for counts in item:
                    for k, n in counts.items():
                        total_counts[k] += n

        with open(output_file, 'w') as f:
            for k, c in sorted(total_counts.items(), key=lambda item: item[1], reverse=True):
                f.write('{}\t{}\n'.format(k, c))


def get_clusters_for_masked_sentences(masked_sentences: List[MaskedSentence]):
    sentences = list(map(lambda ms: ms.sentence, masked_sentences))
    predictions = runner.get_predictions_for_masked_sentences(sentences, 5)
    with TorchPool(25) as pool:
        result = pool.map(get_clusters_for_predictions, chunks(predictions, 5))
    return [item for sl in result for item in sl]

def get_clusters_for_predictions(predictions: List[str]):
    return runner.get_clusters_for_predictions(predictions, 3)


def get_clusters(masked_sentences: List[MaskedSentence]):
    sentence_amount = len(masked_sentences)
    print('Received {} sentences to predict'.format(sentence_amount))
    total = 0.0
    chunk_size = 1000
    for i, chunk in enumerate(chunks(masked_sentences, chunk_size)):
        n_fr = i * chunk_size
        n_to = min(sentence_amount, (i + 1) * chunk_size - 1)
        print('Getting predictions for sentences {:05d}-{:05d}...'.format(n_fr, n_to), end='\t')
        ms_chunks = list(chunks(chunk, GPU_BATCH_SIZE))
        start = time.time()
        # result = map(get_clusters_for_masked_sentences, ms_chunks)
        all_preds = []
        for c in ms_chunks:
            sentences = list(map(lambda ms: ms.sentence, c))
            predictions = runner.get_predictions_for_masked_sentences(sentences, 5)
            all_preds += predictions
        with TorchPool(25) as pool:
            result = pool.map(get_clusters_for_predictions, chunks(all_preds, 5))
        end = time.time()
        print('done ({}s)'.format(round(end - start, 2)))
        total += (end - start)
        
        yield list(zip(itertools.chain.from_iterable(result), itertools.chain.from_iterable(ms_chunks)))
    print('Batch predicted in {}s'.format(round(total, 2)))


def debug(filenames, output_file):
    total_counts = defaultdict(int)
    for filename in filenames:
        print('Loading file {}'.format(filename))
        runner.load_file(filename)
        sentences_in_file = len(runner.text_file)
        sentence_ids_chunked = chunks(range(sentences_in_file), 5000)
        debug_content = []
        for i, s in enumerate(sentence_ids_chunked):
            print('Processing sentence chunks {:06d}-{:06d} of {}'.format(i*5000, (i+1)*5000-1, sentences_in_file))
            masked_sentences = generate_masked(s)
            for result in get_clusters(masked_sentences):
                for clusters, ms in result:
                    debug_content.append((clusters, ms))
                    for cluster in clusters:
                        total_counts[cluster] += 1
        print(len(debug_content))
        with open(os.path.join(runner.ds.text_folder, filename + '_DEBUG_PRED.txt'), 'w') as f:
            for clusters, ms in debug_content:
                f.write('S_ID: {}\n'.format(ms.s_id))
                f.write('{}\n'.format(ms.get_sentence_as_str()))
                f.write('Clusters for masked position: {}\n\n'.format(', '.join(clusters)))
    with open(output_file, 'w') as f:
        for k, c in sorted(total_counts.items(), key=lambda item: item[1], reverse=True):
            f.write('{}\t{}\n'.format(k, c))


def count(lang, model_name, files, write_debug_files, subdir):
    global runner
    ds = DataSource(lang, model_name, subdir=subdir)
    runner = NerRunner(ds)
    out_file_name = 'count_' + '_'.join(files) + '.txt'
    if write_debug_files:
        debug(files, os.path.join(ds.text_folder, out_file_name))
    else:
        count_clusters(files, os.path.join(ds.text_folder, out_file_name))


def get_label_with_cluster(s):
    labels, clusters = runner.get_labels_for_masked_sentences([s], 5, 5, True)
    return labels[0], clusters[0]


def label_sentence(s_id):
    masked_sentences = runner.generate_masked_sentences_for_id(s_id)
    ms_t = [ms.sentence for ms in masked_sentences]
    labels, clusters = runner.get_labels_for_masked_sentences(ms_t, 5, 5, True)
    orig_s = runner.text_file[s_id]
    print(orig_s.text)
    print('\n')
    for ms, label, c in zip(masked_sentences, labels, clusters):
        print(ms.get_sentence_as_str())
        print('Word `{}` has label `{}`. Clusters: {}\n'.format(orig_s.text[ms.offset:ms.end_pos], label, c))
    print('\n\n')


def annotate_predictions(predictions):
    return runner.get_labels_for_predictions(predictions, 3)


def get_annotations(masked_sentences: List[MaskedSentence]) -> List[Annotation]:
    sentences = [ms.sentence for ms in masked_sentences]
    predictions = runner.get_predictions_for_masked_sentences(sentences, 5)
    with TorchPool(25) as pool:
        label_result = pool.map(annotate_predictions, chunks(predictions, 5))
    labels = [item for sentence_labels in label_result for item in sentence_labels]
    result = []
    for ms, l in zip(masked_sentences, labels):
        if l:
            result.append(Annotation(ms, l))
    return result


def label_masked_sentences(masked_sentences: List[MaskedSentence]):
    sentence_amount = len(masked_sentences)
    print('Received {} sentences to predict'.format(sentence_amount))
    total = 0.0
    chunk_size = 1000
    for i, chunk in enumerate(chunks(masked_sentences, chunk_size)):
        n_fr = i * chunk_size
        n_to = min(sentence_amount, (i + 1) * chunk_size - 1)
        print('Getting predictions for sentences {:05d}-{:05d}...'.format(n_fr, n_to), end='\t')
        start = time.time()
        result = list(map(get_annotations, chunks(chunk, GPU_BATCH_SIZE)))
        end = time.time()
        print('done ({}s)'.format(round(end - start, 2)))
        total += (end - start)
        yield list(itertools.chain.from_iterable(result))
    print('Batch predicted in {}s'.format(round(total, 2)))


def label_file(filename):
    print('loading file {}'.format(filename))
    runner.load_file(filename)
    sentences_in_file = len(runner.text_file)
    chunk_size = 5000
    sentence_ids_chunked = chunks(range(sentences_in_file), chunk_size)
    token_id = 1
    partial_files = []
    partial_up_to = 0
    for i, s in enumerate(sentence_ids_chunked):
        n_fr = i * chunk_size
        n_to = min(sentences_in_file, (i + 1) * chunk_size - 1)
        print('Processing sentence chunks {:06d}-{:06d} of {}'.format(n_fr, n_to, sentences_in_file))
        masked_sentences = generate_masked(s)
        for annotations in label_masked_sentences(masked_sentences):
            if len(annotations) == 0:
                print('No annotations for current batch chunk found')
                continue
            partial_up_to += len(annotations)
            part_file_name, token_id = write_partial_file(filename, annotations, partial_up_to, token_id)
            print('Wrote intermediate results to {}'.format(part_file_name))
            partial_files.append(part_file_name)
            if len(partial_files) > 5:
                result_file = merge_partial_files(partial_files)
                print('Merged intermediate results to {}'.format(result_file))
                partial_files = [result_file]
    if len(partial_files) > 1:
        result_file = merge_partial_files(partial_files)
    else:
        result_file = partial_files[0]
    final_file = os.path.join(runner.ds.text_folder, filename + '.ann')
    os.rename(result_file, final_file)
    print('done for file {}, results in {}'.format(filename, final_file))


def merge_partial_files(partial_files):
    first = partial_files[0]
    with open(first, 'a+') as target:
        for file in partial_files[1:]:
            with open(file, 'r') as src:
                content = src.readlines()
            target.writelines(content)
            os.remove(file)
    os.rename(first, partial_files[-1])
    return partial_files[-1]


def write_partial_file(filename, annotations, n_to, token_id):
    out_file_name = runner.ds.get_annotation_filename(filename, n_to)
    with open(out_file_name, 'w') as f:
        for annotation in annotations:
            original_sentence = runner.text_file[annotation.masked_sentence.s_id]
            sentence_offset = original_sentence.offset
            boundaries = annotation.get_boundaries_in_file(sentence_offset)
            line = annotation_line(
                token_id,
                LABELS.get(annotation.label, 'other'),
                boundaries,
                original_sentence.text[annotation.masked_sentence.offset:annotation.masked_sentence.end_pos]
            )
            f.write(line)
            token_id += 1
    return out_file_name, token_id


def annotation_line(annotation_id, label, boundaries, text):
    return 'T{}\t{} {} {}\t{}\n'.format(annotation_id, label, boundaries[0], boundaries[1], text)


def inspect_count(args):
    count(args.language, args.model, args.files, args.debug, args.subdir)


def interact(args):
    ds = DataSource(args.language, args.model, subdir=args.subdir)
    global runner
    runner = NerRunner(ds)
    runner.load_file(args.file)
    read_sentence(label_sentence, len(runner.text_file))


def labeling(args):
    ds = DataSource(args.language, args.model, subdir=args.subdir)
    global runner
    runner = NerRunner(ds)
    for file in args.files:
        label_file(file)


if __name__ == '__main__':
    options = parse_options(sys.argv[1:])
    options.func(options)
