from typing import List, Tuple


class TextFileLine:
    def __init__(self, line_id: int, offset: int, text: str):
        self.line_id = line_id
        self.offset = offset
        self.text = text


class TextFile:
    def __init__(self, lines: List[TextFileLine]):
        self.lines = lines

    def __getitem__(self, item):
        return self.lines[item]

    def __len__(self):
        return len(self.lines)


class PosToken:
    def __init__(self, tag_line):
        parts = tag_line.strip().split('\t')
        self.id = int(parts[0])
        self.token = str(parts[1])
        self.POS_tag = str(parts[2])
        self.rel_id = int(parts[3])
        self.type = str(parts[4])

    def __str__(self):
        return '{:02d}\t{}\t{:02d}\t{:12}\t{}'.format(self.id, self.POS_tag, self.rel_id, self.type, self.token)


class PosSentence:
    def __init__(self, tag_lines):
        self.pos_tokens = list(map(lambda t: PosToken(t), tag_lines))

    def __len__(self):
        return len(self.pos_tokens)

    def __getitem__(self, item) -> PosToken:
        return self.pos_tokens[item]

    def __str__(self):
        return '\n'.join(map(lambda t: str(t), self.pos_tokens))

    def original_sentence(self):
        return ' '.join(map(lambda t: t.token, self.pos_tokens))

    def tokens(self):
        return list(map(lambda t: t.token, self.pos_tokens))


class MaskedSentence:
    def __init__(self, s_id: int, sentence: List[str], offset: int, length: int):
        self.s_id = s_id
        self.sentence = sentence
        self.offset = offset
        self.length = length
        self.end_pos = self.offset + self.length

    def get_sentence_as_str(self):
        return ' '.join(self.sentence)


class Annotation:
    def __init__(self, masked_sentence: MaskedSentence, label: str):
        self.masked_sentence = masked_sentence
        self.label = label

    def get_boundaries_in_file(self, sentence_offset: int) -> Tuple[int, int]:
        offset_in_file = sentence_offset + self.masked_sentence.offset
        return offset_in_file, offset_in_file + self.masked_sentence.length


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


LANGUAGES = ['de', 'el', 'en', 'es', 'fr', 'it', 'sv']


def read_sentence(callback, total_lines):
    while True:
        sentence = input('sentence id 0-{} (:q to quit)> '.format(total_lines - 1))

        if sentence == ':q':
            break
        try:
            s_id = int(sentence)
            if not 0 <= s_id < total_lines:
                raise ValueError('{} is not in allowed range 0-{}'.format(s_id, total_lines-1))
            callback(s_id)
        except ValueError as e:
            print(e)
