from collections import OrderedDict
from typing import List
import sys
import torch
import os
import json
import numpy as np
import math
import time
from argparse import ArgumentParser

from datasource import DataSource
from util import LANGUAGES

word_embeddings_key = 'bert.embeddings.word_embeddings.weight'
NO_LABEL = 'UNLABELLED'


def parse_options(argv):
    parser = ArgumentParser('Clustering utilities')

    subparsers = parser.add_subparsers(help='Cluster vocabulary')
    exp_vocab = subparsers.add_parser('export-vocab', help='Export vocabulary vectors of a model')
    exp_vocab.add_argument('-l', '--language', choices=LANGUAGES, required=True, help='language of the model')
    exp_vocab.add_argument('-m', '--model', required=True, help='name of the model')

    parser_clustering = subparsers.add_parser('cluster-vocab', help='Cluster a models vocabulary')
    parser_clustering.add_argument('-l', '--language', choices=LANGUAGES, required=True, help='language of the model')
    parser_clustering.add_argument('-m', '--model', required=True, help='name of the model')
    parser_clustering.add_argument('-t', '--threshold', default=0.5, type=float, help='similarity threshold for clustering')

    return parser.parse_args(argv)


def dump_word_vectors(ds: DataSource):
    os.makedirs(ds.model_metadata_path, exist_ok=True)
    target_file = ds.model_word_vectors_filename
    model = torch.load(ds.torch_model_filename)
    embeddings = model[word_embeddings_key]
    print('Writing {} vectors with {} weights..'.format(len(embeddings), len(embeddings[0])))
    with open(target_file, 'w') as f:
        f.write(str(embeddings.tolist()))
    print('done')


def show_keys(model_path):
    model = torch.load(os.path.join(model_path, 'pytorch_model.bin'))
    print(model.keys())


def read_word_vectors(vectors_file):
    with open(vectors_file) as f:
        vectors = json.loads(f.read())
    return vectors


def read_vocab(vocab_file, filter_function):
    with open(vocab_file) as fin:
        vocab = {t: i for i, t in filter(filter_function, enumerate(map(lambda t: t.strip(), fin)))}
    print('count of tokens in vocab file: {}'.format(len(vocab)))
    return vocab


def token_start_filter(key):
    return not (str(key).startswith('#') or str(key).startswith('['))


def token_length_filter(key):
    return len(key) > 2


def token_preserve_filter(key, preserve):
    return key in preserve


def vocab_for_clustering_filter(preserve):
    return lambda v: token_start_filter(v[1]) and (token_length_filter(v[1]) or token_preserve_filter(v[1], preserve))


class Clustering:
    def __init__(self, ds: DataSource):
        self.ds = ds
        self.ds.load_word_vectors()
        self.ds.load_tokenizer()
        self.cosine_cache = {}
        self.embeds_cache = {}
        self.embedding_length = len(self.ds.word_vectors[0])

    @staticmethod
    def for_prediction(ds: DataSource):
        c = Clustering(ds)
        c.ds.load_clusters()
        c.ds.load_cluster_labels()
        return c

    def cluster_vocabulary(self, threshold, count_limit):
        self.ds.load_model_vocab()
        picked = set()
        pivots_dict = OrderedDict()
        n = 0
        total = len(self.ds.model_vocab)
        with open(self.ds.picked_vocab_filename, 'w') as f:
            f.write('\n'.join(map(str, self.ds.model_vocab.values())))
        start = time.time()
        print('started at {}'.format(start))
        for k in self.ds.model_vocab:
            if k in picked:
                continue
            picked.add(k)
            sorted_d = self.get_terms_above_threshold(k, threshold, self.ds.model_vocab)
            arr = []
            for s in sorted_d:
                picked.add(s)
                if len(arr) == count_limit:
                    continue
                arr.append(s)
            if len(arr) > 1:
                max_mean_term, max_mean, std_dev, s_dict = self.find_pivot_subgraph(arr)
                if max_mean_term not in pivots_dict:
                    new_key = max_mean_term
                else:
                    print("****Term already a pivot node:", max_mean_term, "key  is :", k)
                    new_key = max_mean_term + "++" + k
                pivots_dict[new_key] = {"key": new_key, "orig": k, "mean": max_mean, "std": std_dev, "terms": arr}
                # print(new_key, max_mean, std_dev, arr)
            n += 1
            if n % 10 == 0:
                print('{:03} of {}'.format(n, total))
        end = time.time()
        print('done, took {}'.format(end - start))
        print('writing file')
        start = time.time()
        with open(self.ds.clustered_vocab_filename, 'w') as f:
            for k in pivots_dict:
                entry = pivots_dict[k]
                f.write('{}\t{}\t{}\t{}\t{}\n'.format(k, entry['orig'], entry['mean'], entry['std'], entry['terms']))
        end = time.time()
        print('done, took {}'.format(end - start))

    def find_pivot_subgraph(self, terms):
        max_mean = -2
        std_dev = 0
        max_mean_term = None
        means_dict = {}
        if len(terms) == 1:
            return terms[0], 1, 0, {terms[0]: 1}
        for i in terms:
            full_score = 0
            count = 0
            full_dict = {}
            for j in terms:
                if i != j:
                    val = self.calc_inner_prod(i, j)
                    full_score += val
                    full_dict[count] = val
                    count += 1
            if len(full_dict) > 0:
                mean = float(full_score) / len(full_dict)
                means_dict[i] = mean
                # print(i,mean)
                if mean > max_mean:
                    max_mean_term = i
                    max_mean = mean
                    std_dev = 0
                    for k in full_dict:
                        std_dev += (full_dict[k] - mean) * (full_dict[k] - mean)
                    std_dev = math.sqrt(std_dev / len(full_dict))
        sorted_d = OrderedDict(sorted(means_dict.items(), key=lambda kv: kv[1], reverse=True))
        return max_mean_term, round(max_mean, 2), round(std_dev, 2), sorted_d

    def get_terms_above_threshold(self, term, threshold, vocab):
        final_dict = {}
        for k in vocab:
            val = self.calc_inner_prod(term, k)
            val = round(val, 2)
            if val > threshold:
                final_dict[k] = val
        sorted_d = OrderedDict(sorted(final_dict.items(), key=lambda kv: kv[1], reverse=True))
        return sorted_d

    def calc_inner_prod(self, text1, text2):
        result = self.get_inner_prod_from_cache(text1, text2)
        if not result:
            vec1 = self.get_embedding(text1)
            vec2 = self.get_embedding(text2)
            if vec1 is None or vec2 is None:
                return 0
            result = np.inner(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))
            if text1 not in self.cosine_cache:
                self.cosine_cache[text1] = {}
            self.cosine_cache[text1][text2] = result
        return result

    def get_inner_prod_from_cache(self, text1, text2):
        if text1 in self.cosine_cache and text2 in self.cosine_cache[text1]:
            return self.cosine_cache[text1][text2]
        elif text2 in self.cosine_cache and text1 in self.cosine_cache[text2]:
            return self.cosine_cache[text2][text1]
        else:
            return None

    def get_embedding(self, text):
        if text in self.embeds_cache:
            return self.embeds_cache[text]
        tokens = self.ds.tokenizer.tokenize(text)
        token_ids = self.ds.tokenizer.convert_tokens_to_ids(tokens)
        vec = self.get_vector_sum(token_ids)
        self.embeds_cache[text] = vec
        return vec

    def get_vector_sum(self, indexed_tokens):
        if len(indexed_tokens) == 0:
            return None
        vectors_sum = np.zeros(self.embedding_length)
        for i, token_index in enumerate(indexed_tokens):
            term_vec = self.ds.word_vectors[token_index]
            vectors_sum += term_vec
        sq_sum = np.sum(vectors_sum ** 2)
        sq_sum = math.sqrt(sq_sum)
        result = vectors_sum / sq_sum
        return result

    def find_clusters(self, words, num_clusters):
        max_mean_term, max_mean, std_dev, s_dict = self.find_pivot_subgraph(words)
        pivot_similarities = {}
        for cluster_key in self.ds.clusters:
            val = round(self.calc_inner_prod(max_mean_term, cluster_key), 2)
            pivot_similarities[cluster_key] = val

        sorted_d = OrderedDict(sorted(pivot_similarities.items(), key=lambda kv: kv[1], reverse=True))
        result = list(sorted_d.keys())[:num_clusters]
        return result

    def get_cluster_labels(self, cluster_pivots: List[str]) -> List[str]:
        return [self.get_cluster_label(c) for c in cluster_pivots]

    def get_cluster_label(self, cluster_pivot: str) -> str:
        return self.ds.cluster_labels.get(cluster_pivot, NO_LABEL)


if __name__ == '__main__':
    options = parse_options(sys.argv[1:])
    ds = DataSource(options.language, options.model)
    c = Clustering(ds)
    c.cluster_vocabulary()
