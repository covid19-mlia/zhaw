import numpy as np
from pathlib import Path

class Ann:
    def __init__(self, fname):
        self.all = read_ann(fname)
        self.fname = Path(fname)
    
    def drugs(self):
        return self.get('drug-trt')
    def sosy(self):
        return self.get('sosy-dis')
    def behav(self):
        return self.get('behavior')
    def legal(self):
        return self.get('legal-reg')
    def findings(self):
        return self.get('findings')
    def tests(self):
        return self.get('tests')
    def get(self, l):
        return self.all.get(l, [])
    def count_all(self):
        return sum(map(len, self.all.values()))
    def print(self):
        print('{:>10.10}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}'.format(self.fname.stem, len(self.drugs()), len(self.sosy()), len(self.behav()), len(self.legal()), len(self.findings()), len(self.tests()), self.count_all()))
    def numpy(self):
        return np.array([len(self.drugs()), len(self.sosy()), len(self.behav()), len(self.legal()), len(self.findings()), len(self.tests())])
def read_ann(fname):
    result = {}
    with open(fname, 'r') as f:
        for line in f:
            _, ann, text = line.split('\t')
            label, start, end = ann.split()
            if label not in result:
                result[label] = []
            result[label] += [[start, end, text]]
    return result

def print_ann_summary(anns):
    print('{:>10}\tD\tS\tB\tL\tF\tT\ttotal'.format('filename'))
    totals = []
    for a in anns:
        a.print()
        totals.append(a.numpy())
    per_cat = np.sum(totals, axis=0)
    print('{:>10}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}\t{:4d}'.format('total:', *list(per_cat), np.sum(per_cat)))

def read_annotation_files(d):
    p = Path(d)
    return [Ann(af.as_posix()) for af in p.glob('*.ann')]
