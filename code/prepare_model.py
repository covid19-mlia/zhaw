from argparse import ArgumentParser

from clustering import dump_word_vectors, Clustering
import sys

from datasource import DataSource
from util import LANGUAGES


def parse_options(args):
    parser = ArgumentParser('Prepare model')
    parser.add_argument('language', metavar='LANG', choices=LANGUAGES, help='language of the model')
    parser.add_argument('model', metavar='MODELNAME', help='name of the model')
    parser.add_argument('-t', '--threshold', default=0.5, type=float, help='similarity threshold for clustering')
    parser.add_argument('-s', '--cluster_size', default=10, type=int, help='size of clusters (words)')
    return parser.parse_args(args)


if __name__ == '__main__':
    options = parse_options(sys.argv[1:])
    if options.language and options.model:
        ds = DataSource(options.language, options.model)
        dump_word_vectors(ds)
        c = Clustering(ds)
        print('Clustering vocabulary')
        c.cluster_vocabulary(options.threshold, options.cluster_size)
