from typing import List

import torch
import numpy as np
import os

from datasource import DataSource

CUDA_DEVICE = 'cuda:0'

def read_picked_vocab_terms(base_path):
    with open(os.path.join(base_path, 'clustering', 'picked_vocab_terms.txt'), 'r') as f:
        result = np.array(list(map(int, f.readlines())))
    return result


class MaskedWordPredictor:
    def __init__(self, ds: DataSource):
        ds.load_tokenizer()
        ds.load_model(CUDA_DEVICE)
        ds.load_picked_vocab()
        self.tokenizer = ds.tokenizer
        self.model = ds.model
        self.allowed_terms = ds.picked_vocab
        self.model.eval()
        self.ds = ds

    def predict_masked_words(self, sentences: List[List[str]], top_k=5):
        tokenized = self.sentences_to_model_input(sentences)

        try:
            mask_indices = np.array(
                list(map(lambda st: torch.nonzero(st == self.tokenizer.mask_token_id).squeeze().item(),
                         tokenized['input_ids']))
            )
        except ValueError:
            print('No mask in sentence')
            return []
        with torch.no_grad():
            output = np.array(self.model(**tokenized)[0].to('cpu'))
            del tokenized
            torch.cuda.empty_cache()

        all_terms = output[np.arange(len(mask_indices)), mask_indices][:, self.allowed_terms]
        top_indices = np.argsort(all_terms, axis=1)[:, -1:-top_k-1:-1]
        predictions = [self.tokenizer.convert_ids_to_tokens(self.allowed_terms[ti]) for ti in top_indices]
        return predictions

    def sentences_to_model_input(self, sentences: List[List[str]]):
        tokenized = self.tokenizer(
            sentences,
            add_special_tokens=True,
            is_split_into_words=True,
            padding='longest',
            truncation=True,
            max_length=512,
            return_tensors='pt'
        )
        for k, v in tokenized.items():
            tokenized[k] = v.to(CUDA_DEVICE)
        return tokenized

    def predict_masked_word(self, sentence: str, top_k=20):
        tokenized = self.sentences_to_model_input([sentence.split()])

        try:
            mask_index = (tokenized['input_ids'][0] == self.tokenizer.mask_token_id).nonzero().squeeze().item()
        except ValueError:
            print('No mask in sentence')
            return []

        with torch.no_grad():
            predictions = np.array(self.model(**tokenized)[0].to('cpu'))
            del tokenized
            torch.cuda.empty_cache()

        scores = np.array(predictions[0][mask_index])
        top_ids = np.argsort(scores)[-1:-top_k - 1:-1]
        result = list(zip(scores[top_ids], self.tokenizer.convert_ids_to_tokens(top_ids)))
        return result
