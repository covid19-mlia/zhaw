# Unsupervised NER using BERT
This directory contains the code used for the first Task of the Covid-19 MLIA Evaluation effort (http://eval.covid19-mlia.eu/task1/)

Based on Ajit Rajasekharan's "Unsupervised NER using BERT"-Method (https://github.com/ajitrajasekharan/unsupervised_NER). Uses some of the code almost unchanged, other parts were adjusted or rewritten as necessary.

The details of the method are described in this blog post: https://towardsdatascience.com/unsupervised-ner-using-bert-2d7af5f90b8a

## Main steps:
1. Create POS-Tags for input files (see pos_tagger directory). POS-Tags for languages DE/EL/EN/ES are available in the `resource`-folder (EL and ES are incomplete; the larger files weren't processed).
1. Create the conda-environment from `mlia_bert.yml`
1. Adjust paths in the `DataSource` class (`datasource.py`) to your structure. They should point to directories containing:
    - the input text documents
    - the files containing the POS-Tags for the documents
    - the downloaded BERT models
     
     The directory with the BERT-models should consist of one folder per language, inside each language one folder per
      model.
      
1. Download one or more BERT-models for masked word prediction for the desired language(s) from https://huggingface.co/models
1. Run `prepare_model.py` with a language and modelname, e.g. `python prepare_model.py de medbert`. This will dump the models vocab and cluster it as described in the blog post.
1. To test masked word sentence generation, run `test_ms.py` with a language, modelname and filename (without .txt-extension).
Currently, only experimental sentence generators for DE/EL/EN/ES are implemented.
1. Add some cluster labels for a specific model in the file configured in `DataSource.cluster_labels_filename`. Some labels used for the first MLIA task are available in the `resource` folder for languages DE/EL/EN/ES.
1. To test masked word prediction, count predicted clusters or annotate files, refer to `ner_runner.py -h`.

## Results
The code above was used to create some annotations for the following languages:

*  English, using the model `bert-large-cased` (https://huggingface.co/bert-large-cased)
*  German, using the model `smanjil/German-MedBERT` (https://huggingface.co/smanjil/German-MedBERT)
*  Greek, using the model `nlpaueb/bert-base-greek-uncased-v1` (https://huggingface.co/nlpaueb/bert-base-greek-uncased-v1)
*  Spanish, using the model `dccuchile/bert-base-spanish-wwm-cased` (https://huggingface.co/dccuchile/bert-base-spanish-wwm-cased)
   
Results are in the folder `submission/task1/round1`. Due to resource limitations, the larger text files were not processed.