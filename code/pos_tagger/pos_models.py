import json
import sys
import os
import gdown
from argparse import ArgumentParser

MODELS_PATH = 'models'


def parse_options(argv, languages):
    parser = ArgumentParser('Download POS tagging models')
    parser.add_argument('-f', '--force', action='store_true', default=False,
                        help='force download (overwrite existing model)')
    parser.add_argument('-l', '--languages', nargs='+', choices=languages, required=True)

    return parser.parse_args(argv)


def download_models(languages, overwrite, urls):
    for l in languages:
        download_model(l, overwrite, urls)


def download_model(language, overwrite, urls):
    lang_urls = urls[language]
    model_url = get_key('model', lang_urls, True)
    params_url = get_key('params', lang_urls, True)
    if model_url and params_url:
        path = os.path.join(MODELS_PATH, language)
        if not os.path.exists(path):
            os.makedirs(path)
        files = set(os.listdir(path))
        if 'model' not in files or overwrite:
            print('Downloading {} model to {}'.format(language, path))
            gdown.download(model_url, os.path.join(path, 'model'), quiet=False)
        else:
            print('Skipping download of {} model (already exists)'.format(language))
        if 'model.params' not in files or overwrite:
            print('Downloading {} model params to {}'.format(language, path))
            gdown.download(params_url, os.path.join(path, 'model.params'), quiet=False)
        else:
            print('Skipping download of {} model params (already exists)'.format(language))


def get_key(key, json, print_error):
    value = json.get(key, )
    if not value and print_error:
        print("ERROR: key {} not found in {}".format(key, json))
    return value


if __name__ == '__main__':
    with open('pos_model_urls.json', 'r') as f:
        urls = json.load(f)
    options = parse_options(sys.argv[1:], urls.keys())
    download_models(options.languages, options.force, urls)
