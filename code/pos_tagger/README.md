Needs to be run in a python2.7 environment. If using conda you can use `conda env create --file mlia_pos.yml` to create a working environment.

Uses datquocnguyen's Joint POS Tagging and Dependency Parsing implementation from here: https://github.com/datquocnguyen/jPTDP.git

`pos_models.py`: download POS-Models from the URL specified in `pos_model_urls.json`

e.g. `pos_models.py en`

`pos_tagger.py LANG`: run POS-Tagging in interactive mode for language LANG.

`pos_tagger.py LANG SRC_DIR TARGET_DIR`: run POS-Tagging files with `.txt`-extension in SRC_DIR and write results to TARGET_DIR with extension `.pos.txt`. Uses all available CPU-cores.