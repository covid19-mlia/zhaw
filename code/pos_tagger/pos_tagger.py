import sys
import os
import tempfile
import re
from multiprocessing import Pool
from jPTDP.funcs import chunks

model = None
target_dir = ''
BATCH_SIZE = 5000
SENTENCES_PER_THREAD = 100


def load_model(model_path):
    from jPTDP.jPTDP import load_pretrained
    return load_pretrained(model_path)


def create_pos_tags(sentences):
    inp_file = tempfile.NamedTemporaryFile(delete=False).name
    with open(inp_file, 'w') as f_in:
        f_in.writelines(sentences)
    out_file = tempfile.NamedTemporaryFile(dir=target_dir, delete=False).name
    model.PredictFromText(inp_file, out_file)
    os.remove(inp_file)
    return out_file


def bulk_mode(src_dir, out_dir, lang):
    global model, target_dir
    target_dir = out_dir
    model = load_model(get_model_path(lang))
    files = [f for f in os.listdir(src_dir) if re.match(r'[^.]+\.txt\Z', f)]
    print('Found {} files to process'.format(len(files)))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for f in files:
        source_file = os.path.join(src_dir, f)
        target_file = os.path.join(target_dir, re.sub(r'\.txt\Z', '.pos.txt', f))
        print('Tagging file {} to file {}'.format(source_file, target_file))
        tag_file(source_file, target_file)


def tag_file(source_file, target_file):
    with open(source_file, 'r') as f:
        content = list(filter(lambda s: not s.isspace(), f.readlines()))
    total_lines = len(content)
    lines = chunks(content, BATCH_SIZE)
    for i, l in enumerate(lines):
        print('Processing lines {}-{} of {}'.format(i * BATCH_SIZE, (i + 1) * BATCH_SIZE, total_lines))
        async_chunks = chunks(l, SENTENCES_PER_THREAD)
        try:
            p = Pool()
            tmp_files = p.map(create_pos_tags, async_chunks)
            with open(target_file, 'a+') as out:
                for tmp in tmp_files:
                    with open(tmp, 'r') as t:
                        out.writelines(t.readlines())
                    os.remove(tmp)
            p.close()
        except Exception as e:
            if p:
                p.close()
            print(e)


def interactive_mode(model_path):
    model = load_model(model_path)
    callback = lambda s: pos_tag_sentence(model, s)
    read_sentence(callback)


def pos_tag_sentence(model, sentence):
    i, o = create_temp_files_for_sentence(sentence)
    model.PredictFromText(i, o)
    with open(o, 'r') as ofp:
        result = ofp.read()
    print(result)
    os.remove(i)
    os.remove(o)


def create_temp_files_for_sentence(sentence):
    inp = tempfile.NamedTemporaryFile(dir='.', delete=False).name
    out = tempfile.NamedTemporaryFile(dir='.', delete=False).name
    with open(inp, 'w') as ifp:
        ifp.write(sentence)
    return inp, out


def read_sentence(callback):
    while True:
        # for python 2
        sentence = raw_input("sentence (:q to quit)> ")
        print(sentence)

        if sentence == ":q":
            break

        callback(sentence)


def get_model_path(lang):
    return os.path.join('models', lang, 'model')


if __name__ == '__main__':
    if len(sys.argv) == 2:
        interactive_mode(get_model_path(sys.argv[1]))
    elif len(sys.argv) == 4:
        bulk_mode(sys.argv[2], sys.argv[3], sys.argv[1])
    else:
        print("\nspecify language for interactive mode, or language, source and target dir for bulk file mode\n")
