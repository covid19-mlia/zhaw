from typing import List

import unicodedata
import re

from util import TextFileLine, PosSentence, MaskedSentence, PosToken

MASK = '[MASK]'
DE_NOUNS = {'PROPN', 'NOUN'}
EN_NOUNS = {'NFP', 'JJ', 'NN', 'FW', 'NNS', 'NNPS', 'JJS', 'JJR', 'NNP', 'POS', 'CD'}
EL_NOUNS = {'PROPN', 'NOUN', 'X'}
ES_NOUNS = {'PROPN', 'NOUN', 'ADJ', 'VERB'}


def gen_sentences(lang: str, s: TextFileLine, pos: PosSentence) -> List[MaskedSentence]:
    result = []
    if lang == 'de':
        result = mask_single_nouns(s, pos, lang, DE_NOUNS)
    elif lang == 'el':
        result = mask_noun_spans(s, pos, lang, EL_NOUNS)
    elif lang == 'en':
        result = mask_noun_spans(s, pos, lang, EN_NOUNS)
    elif lang == 'es':
        result = mask_noun_spans(s, pos, lang, ES_NOUNS)
    return result


def mask_single_nouns(line: TextFileLine, pos: PosSentence, lang, noun_tags) -> List[MaskedSentence]:
    sentences = []
    tokens = preprocess_tokens(pos.tokens(), lang)
    current_offset = 0
    original_sentence = line.text
    for i, pos_token in enumerate(pos.pos_tokens):
        before_start_len = 0
        before_start = re.findall(r'\A(\W+)(?:{})'.format(re.escape(pos_token.token)), original_sentence)
        if before_start:
            before_start_len = len(before_start[0])
        current_offset += before_start_len
        if pos_token.POS_tag in noun_tags:
            start, token, end = split_token(pos_token.token)
            before_mask = tokens[:i]
            after_mask = tokens[i+1:]
            sentence = before_mask + ['{}{}{}'.format(start, MASK, end)] + after_mask
            sentences.append(MaskedSentence(line.line_id, sentence, current_offset + len(start), len(token)))
        current_offset += len(pos_token.token)
        original_sentence = original_sentence[before_start_len+len(pos_token.token):]
    return sentences


def mask_noun_spans(line: TextFileLine, pos: PosSentence, lang, noun_tags) -> List[MaskedSentence]:
    size = len(pos)
    sentence_arr = []
    i = 0
    original_sentence = line.text
    current_offset = 0
    tokens = preprocess_tokens(pos.tokens(), lang)
    while i < size:
        term_info = pos[i]
        before_start_len = 0
        before_start = re.findall(r'\A(\W+)(?:{})'.format(re.escape(term_info.token)), original_sentence)
        if before_start:
            before_start_len = len(before_start[0])
        current_offset += before_start_len
        original_sentence = original_sentence[before_start_len:]
        if len(term_info.token) > 1 and term_info.POS_tag in noun_tags:
            start_tokens = tokens[:i]
            noun_span = get_noun_span(i, term_info, pos, original_sentence, noun_tags)
            i, span_start, span_end, total_len, starting_punct, ending_punct = noun_span
            end_tokens = tokens[i:]
            masked_span = '{}{}{}'.format(starting_punct, MASK, ending_punct)
            sentence = start_tokens + [masked_span] + end_tokens
            ms = MaskedSentence(line.line_id, sentence, current_offset + span_start, span_end - span_start)
            sentence_arr.append(ms)
            current_offset += total_len
            original_sentence = original_sentence[total_len:]
        else:
            i += 1
            current_offset += len(term_info.token)
            original_sentence = original_sentence[len(term_info.token):]

    return sentence_arr


def get_noun_span(i, start_token, pos, original_sentence, noun_tags):
    start, token, end = split_token(start_token.token)
    span_start = len(start)
    total_len = len(start_token.token)
    token_len = len(token)
    if len(end) > 0:
        span_end = span_start + token_len
        return i + 1, span_start, span_end, total_len, start, end
    elif start_token.POS_tag == 'VERB':
        span_end = span_start + token_len
        return i + 1, span_start, span_end, total_len, start, end
    j = i + 1
    original_sentence = original_sentence[total_len:]
    e = None
    prev_e = ''
    while j < len(pos):
        term_info = pos[j]
        before_start_len = 0
        before_start = re.findall(r'\A(\W+)(?:{})'.format(re.escape(term_info.token)), original_sentence)
        if before_start:
            before_start_len = len(before_start[0])
        original_sentence = original_sentence[before_start_len:]
        s, t, e = split_token(term_info.token)
        if len(t) == 0:
            return j + 1, span_start, total_len, total_len, '', ''
        elif len(s) > 0 or term_info.POS_tag not in noun_tags or is_standalone_verb(term_info):
            return j, span_start, total_len, total_len, start, prev_e
        elif len(e) > 0 or term_info.POS_tag == 'VERB':
            span_end = total_len + before_start_len + len(t)
            return j + 1, span_start, span_end, span_end + len(e), start, e
        else:
            j += 1
            total_len += before_start_len + len(term_info.token)
            original_sentence = original_sentence[len(term_info.token):]
            prev_e = e
    if e:
        ending_punct = e
    elif j == i+1:
        ending_punct = end
    else:
        ending_punct = ''
    return j, span_start, total_len, total_len, start, ending_punct


def normalize_casing_of_tokens(tokens):
    return list(map(lambda t: t[0] + t[1:].lower() if len(t) > 0 else t, tokens))


def split_token(token):
    non_word_start_match = re.search(r'\A(\W+)', token)
    non_word_end_match = re.search(r'(\W+)\Z', token)
    if non_word_start_match:
        start_span = non_word_start_match.span()
    else:
        start_span = (0, 0)
    if non_word_end_match:
        end_span = non_word_end_match.span()
    else:
        end_span = (len(token), len(token))
    return token[start_span[0]:start_span[1]], token[start_span[1]:end_span[0]], token[end_span[0]:end_span[1]]


def preprocess_tokens(tokens: List[str], lang) -> List[str]:
    if lang == 'el':
        return list(map(strip_accents_and_lowercase, tokens))
    else:
        return normalize_casing_of_tokens(tokens)


def strip_accents_and_lowercase(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn').lower()


def is_standalone_verb(term_info):
    return term_info.POS_tag == 'VERB' and term_info.type in {'root', 'acl'}