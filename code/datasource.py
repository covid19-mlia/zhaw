from typing import List, Tuple
import os
import re
from transformers import AutoTokenizer, BertForMaskedLM, AutoModelForMaskedLM
import numpy as np
import json

from util import TextFileLine, TextFile, PosSentence

MODEL_META_PATH = 'clustering'


class DataSource:
    def __init__(self, lang, model_name, base_path='', subdir=''):
        self.base_path = base_path
        self.lang = lang
        self.text_folder = os.path.join(base_path, lang, subdir)
        self.pos_folder = os.path.join(self.text_folder, 'pos')
        self.model_path = os.path.join(base_path, 'bert_models', lang, model_name)
        self.torch_model_filename = os.path.join(self.model_path, 'pytorch_model.bin')
        self.model_vocab_filename = os.path.join(self.model_path, 'vocab.txt')
        self.model_metadata_path = os.path.join(self.model_path, MODEL_META_PATH)
        self.clustered_vocab_filename = os.path.join(self.model_metadata_path, 'clusters.txt')
        self.cluster_labels_filename = os.path.join(self.model_metadata_path, 'cluster_labels.txt')
        self.picked_vocab_filename = os.path.join(self.model_metadata_path, 'picked_vocab_terms.txt')
        self.model_word_vectors_filename = os.path.join(self.model_metadata_path, 'word_vectors')
        self.preserve_1_2_grams_filename = 'preserve_1_2_grams.txt'
        self.tokenizer = None
        self.model = None
        self.picked_vocab = None
        self.word_vectors = None
        self.model_vocab = None
        self.grams_to_preserve = None
        self.clusters = None
        self.cluster_labels = None
        self.file_content = {}

    def load_tokenizer(self):
        if not self.tokenizer:
            self.tokenizer = AutoTokenizer.from_pretrained(self.model_path, do_lower_case=False)

    def load_model(self, cuda_device=None):
        if not self.model:
            self.model = AutoModelForMaskedLM.from_pretrained(self.model_path)
        if cuda_device:
            self.model = self.model.to(cuda_device)

    def load_picked_vocab(self):
        if not self.picked_vocab:
            self.picked_vocab = read_picked_vocab_terms(self.picked_vocab_filename)

    def load_word_vectors(self):
        if not self.word_vectors:
            self.word_vectors = read_word_vectors(self.model_word_vectors_filename)

    def load_model_vocab(self):
        if not self.model_vocab:
            self.load_grams_to_preserve()
            self.model_vocab = read_vocab(
                self.model_vocab_filename,
                vocab_for_clustering_filter(self.grams_to_preserve)
            )

    def load_grams_to_preserve(self):
        if not self.grams_to_preserve:
            self.grams_to_preserve = read_1_2_grams(self.preserve_1_2_grams_filename)

    def load_clusters(self):
        if not self.clusters:
            self.clusters = read_clusters(self.clustered_vocab_filename)

    def load_cluster_labels(self):
        if not self.cluster_labels:
            self.cluster_labels = read_cluster_labels(self.cluster_labels_filename)

    def list_source_files(self):
        return list(
            map(
                lambda fn: re.sub(r'\.txt\Z', '', fn),
                filter(lambda fn: re.match(r'\d{4}\.txt\Z', fn), os.listdir(self.text_folder))
            )
        )

    def get_text_file(self, filename, cache=False) -> Tuple[TextFile, List[PosSentence]]:
        if filename not in self.file_content:
            original_content = load_text_file(os.path.join(self.text_folder, filename + '.txt'))
            pos_info = read_pos_file(os.path.join(self.pos_folder, filename + '.pos.txt'))
            if not cache:
                return original_content, list(pos_info)
            self.file_content[filename] = (original_content, list(pos_info))
        return self.file_content[filename]

    def get_annotation_filename(self, file, upto=None):
        if upto:
            filename = '{}.partial-{}.ann'.format(file, str(upto))
        else:
            filename = '{}.ann'.format(file)
        return os.path.join(self.text_folder, filename)


def read_picked_vocab_terms(filename):
    try:
        with open(filename, 'r') as f:
            result = np.array(list(map(int, f.readlines())))
        return result
    except FileNotFoundError:
        print('No picked vocabulary found for current model')
        return None


def read_word_vectors(vectors_file):
    with open(vectors_file) as f:
        vectors = json.loads(f.read())
    return vectors


def vocab_for_clustering_filter(preserve):
    return lambda v: token_start_filter(v[1]) and (token_length_filter(v[1]) or token_preserve_filter(v[1], preserve))


def token_start_filter(key):
    return not (str(key).startswith('#') or str(key).startswith('['))


def token_length_filter(key):
    return len(key) > 2


def token_preserve_filter(key, preserve):
    return key in preserve


def read_vocab(vocab_file, filter_function):
    with open(vocab_file) as fin:
        vocab = {t: i for i, t in filter(filter_function, enumerate(map(lambda t: t.strip(), fin)))}
    print('count of tokens in vocab file: {}'.format(len(vocab)))
    return vocab


def read_1_2_grams(filename):
    if not os.path.exists(filename):
        print('File {} not found, not loading any 1_2grams to preserve'.format(filename))
        return []
    with open(filename, 'r') as p:
        return set(p.readlines())


def read_clusters(filename):
    with open(filename, 'r') as f:
        result = {
            line[1]: {
                'aux': line[0],
                'mean': line[2],
                'std': line[3],
                'cluster': line[4]
            } for line in map(lambda l: l.split('\t'), f)
        }
    return result


def read_cluster_labels(filename):
    if not os.path.exists(filename):
        print('No cluster labels found at {}'.format(filename))
        return {}
    with open(filename, 'r') as f:
        result = {line[0]: line[1] for line in map(lambda l: l.strip().split('\t'), filter(lambda l: not l.isspace(), f))}
    return result


def load_text_file(filename) -> TextFile:
    with open(filename, 'r') as f:
        current_offset = 0
        lines = []
        i = 0
        for line in f:
            if not line.isspace():
                lines.append(TextFileLine(i, current_offset, line))
                i += 1
            current_offset += len(line)
        return TextFile(lines)


def read_pos_file(pos_filename):
    tags = []
    with open(pos_filename, 'r') as f:
        for line in f:
            if line.strip() == '':
                yield PosSentence(tags)
                tags = []
            else:
                tags.append(line)
    if len(tags) > 0:
        yield PosSentence(tags)
